const express = require('express');
const fetch = require('cross-fetch');

const router = express.Router();

const baseUrl = 'https://api.yalidine.app/v1/communes/?wilaya_id=';
const apiId = '43907181276623609043';
const apiToken = 'TjgVCsxhB0YfaGtabr3jyMIUSiFk2AgE17xuoymiqMQs0pJonOlTN4w4zX1H6SRc';

const getCommune = async (req, res) => {
    const { wilaya_id } = req.body;
    try {
        const response = await fetch(`${baseUrl}${wilaya_id}`, {
            method: 'GET',
            headers: {
                'content-type': 'application/json',
                'X-API-ID': apiId,
                'X-API-TOKEN': apiToken
            }
        });
        const data = await response.json();
        res.status(200).json(data);
        
    } catch (error) {
        console.error(error.message);
        res.status(500).json({ message: 'An error occurred while fetching communes' });
    };
};

router.post('/', getCommune);

module.exports = router;

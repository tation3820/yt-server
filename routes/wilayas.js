const express = require('express');
const fetch = require('cross-fetch');

const router = express.Router();

const url = 'https://api.yalidine.app/v1/wilayas/?order_by=name';
const apiId = '43907181276623609043';
const apiToken = 'TjgVCsxhB0YfaGtabr3jyMIUSiFk2AgE17xuoymiqMQs0pJonOlTN4w4zX1H6SRc';

const getWilayas = async (req, res) => {
    try {
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'X-API-ID': apiId,
                'X-API-TOKEN': apiToken
            }
        });
        const data = await response.json();
        if (response.ok) {
            res.status(200).json(data);
        }
    } catch (error) {
        console.error(error.message);
        res.status(500).json({ message: 'An error occurred while fetching communes' });
    };
};

router.get('/', getWilayas);

module.exports = router;

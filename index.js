const express = require('express');
const wilayasRoute = require('./routes/wilayas');
const communeRoute = require('./routes/commune');
const cors = require("cors");

require('dotenv').config();

const app = express();

app.use(cors());

const port = process.env.PORT || 3001;

app.use(express.json());
app.use((req, res, next) => {
    console.log(req.path, req.method);
    next()
});

app.use('/api/wilayas/', wilayasRoute);
app.use('/api/commune/', communeRoute);

app.listen(port, () => { console.log(`Running at port ${port}`) });
